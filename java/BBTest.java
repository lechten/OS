public class BBTest
{
    public static void main(String args[]) {
        if (args.length == 0) {
            System.err.println("Need an argument!");
            System.exit(1);
        }
        BoundedBuffer buffer;
        if (args[0].equals("semaphore")) buffer = new SemaphoreBoundedBuffer();
        else buffer = new SynchronizedBoundedBuffer();

        Thread producerThread1 = new Thread(new Producer(buffer, "Producer1"));
        Thread producerThread2 = new Thread(new Producer(buffer, "Producer2"));
        Thread producerThread3 = new Thread(new Producer(buffer, "Producer3"));
        Thread producerThread4 = new Thread(new Producer(buffer, "Producer4"));
        Thread producerThread5 = new Thread(new Producer(buffer, "Producer5"));
        Thread producerThread6 = new Thread(new Producer(buffer, "Producer6"));
        Thread consumerThread1 = new Thread(new Consumer(buffer, "Consumer1"));
        Thread consumerThread2 = new Thread(new Consumer(buffer, "Consumer2"));

        consumerThread1.start();
        consumerThread2.start();
        producerThread1.start();
        producerThread2.start();
        producerThread3.start();
        producerThread4.start();
        producerThread5.start();
        producerThread6.start();
    }
}
