/**
 * A bounded buffer is a classical shared data structure in so-called
 * producer-consumer scenarios, where one or more producer threads
 * create data (usually associated with compute tasks), which are then
 * processed by one or more consumer threads.
 *
 * The basic operations of a bounded buffer are
 * <code>insert()</code>, which inserts an item into the buffer, and
 * <code>retrieve()</code>, which removes an item from the buffer and
 * returns it.  Such buffers are bounded in the sense that they have
 * limited capacity.  Thus, a buffer may be full, which prevents
 * further <code>insert()</code> operations from completing (until
 * a place is freed by <code>retrieve()</code>).  Also,
 * <code>retrieve()</code> can only complete if the buffer is not
 * currently empty.
 *
 * The safe implementation of both methods with mutual exclusion is a
 * topic in its own right.  (See classes
 * <a href="https://gitlab.com/oer/OS/-/blob/master/java/SynchronizedBoundedBuffer.java">SynchronizedBoundedBuffer</a> and
 * <a href="https://gitlab.com/oer/OS/-/blob/master/java/SemaphoreBoundedBuffer.java">SemaphoreBoundedBuffer</a>.)
 */
public interface BoundedBuffer {
    public void insert(Object o) throws InterruptedException;
    public Object retrieve() throws InterruptedException;
    public int size();
}
