# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2018 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

* Research on Motivation

** Self-Regulation
   :PROPERTIES:
   :CUSTOM_ID: self-regulation
   :END:
   #+ATTR_REVEAL: :frag (appear)
   - “Self-regulation is the process by which people change their
     beliefs and actions in the pursuit of their goals”
     #+ATTR_REVEAL: :frag appear
     - What are your *goals*?
   - Self-regulation “is known to support achievement in academic settings”
     #+ATTR_REVEAL: :frag appear
     - When and how do you *plan to engage* with course content?

   #+ATTR_HTML: :class slide-source
   (Source: cite:KC17)

** Goals
   - Individuals strive for different classes of achievement goals:
     mastery vs performance
     #+ATTR_REVEAL: :frag (appear)
     - *Mastery* (or learning) goals
       - Desire to develop *understanding*, improve *skills*
       - Belief that higher effort leads to better outcome
       - “I belong here”
     - *Performance* goals
       - Performance-approach goals
	 - Focus on positive outcomes, desire to perform *better than
           others*, paired with public recognition
	 - *Positive* effect on graded performance
       - Performance-avoidance goals
	 - Grounded in fear of failure, low competency expectancies
	 - *Negative* effect on graded performance

   #+ATTR_HTML: :class slide-source
   (Source: cite:Ame92,EC97)

** Mental Contrasting with Implementation Intentions (MCII)
   :PROPERTIES:
   :CUSTOM_ID: MCII
   :END:
   - Two complementary [[#self-regulation][self-regulation]] strategies
     1. *Mental Contrasting* (MC)
	- Goal commit and goal striving
	- Elaborate on positive outcomes of goals and potential obstacles
     2. *Implementation Intentions* (II)
	- Plan how to overcome obstacles
	- Create if-then plans, in particular for
	  1. getting started and
	  2. staying on track

   #+ATTR_HTML: :class slide-source
   (Source: cite:KC17)

*** MCII Intervention
    - MCII as *online survey* in Learnweb
      - Your participation serves as exercise in
	[[#self-regulation][self-regulation]].
      - Studies demonstrate more successful goal pursuit after MCII
	interventions in various settings (cite:KC17,DGL+11).
    - That survey is a *precondition* for access to material in Learnweb.
      - We review your responses to ensure “serious” answers.
	- Your answers do *not* have any influence on our grading!
      - We plan to repeat such surveys.
