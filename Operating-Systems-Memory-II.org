# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+MACRO: jittquiz [[https://sso.uni-muenster.de/LearnWeb/learnweb2/course/view.php?id=60751#section-11][Learnweb]]

#+TITLE: OS09: Virtual Memory II
#+SUBTITLE: Based on Chapter 6 of cite:Hai19
#+KEYWORDS: operating system, virtual memory, paging, swapping, inverted page table, translation lookaside buffer, TLB, page replacement, clock, FIFO, LRU
#+DESCRIPTION: Second of two presentations on virtual memory of operating systems.  Discusses virtual memory with paging and page replacement

* Introduction

# This is the tenth presentation for this course.
#+CALL: generate-plan(number=10)

** Today’s Core Questions
   - How can the size of page tables be reduced?
   - How can address translation be sped up?
   - How does the OS allocate frames to processes?

** Learning Objectives
   - Explain paging, swapping, and thrashing
   - Discuss differences of different types of page tables
   - Explain role of TLB in address translation
   - Apply page replacement with FIFO, LRU, Clock

** Retrieval Practice
*** Recall: Hash Tables
    :PROPERTIES:
    :CUSTOM_ID: hashing-reminder
    :END:
    #+INDEX: Hashing!Hashing reminder (Memory II)
    - [[https://en.wikipedia.org/wiki/Hash_table][Hash table]] = data
      structure with search in O(1) on average
      - Taught in Data Structures and Algorithms
    - What are hash collisions, buckets, chaining?

*** Previously on OS …
    - [[file:Operating-Systems-Memory-I.org::#main-concepts][What is a virtual address, how is it related to page tables?]]
      - What [[file:Operating-Systems-Memory-I.org::#mmu][piece of hardware]] is responsible for address translation?
    - How [[file:Operating-Systems-Memory-I.org::#page-table-sizes][large are page tables]]?  How many exist?
    - What happens upon [[file:Operating-Systems-Memory-I.org::#page-fault-handler][page misses]]?
    - What is [[file:Operating-Systems-Memory-I.org::#demand-loading][demand loading]]?

*** Selected Questions
    :PROPERTIES:
    :CUSTOM_ID: quiz-memory
    :END:
#+REVEAL_HTML: <script data-quiz="quizMemory" src="./quizzes/memory.js"></script>

** Table of Contents
   :PROPERTIES:
   :UNNUMBERED: notoc
   :END:
#+REVEAL_TOC: headlines 1

* Inverted Page Tables and Hardware Support

** Inverted Page Tables
   :PROPERTIES:
   :CUSTOM_ID: inverted-page-tables
   :END:
   #+INDEX: Page table!Inverted (Memory II)
   #+INDEX: Inverted page table (Memory II)
   #+INDEX: Hashing!Inverted page table (Memory II)
   - Recall: Page tables can be huge, per process
   - Key insights to reduce amount of memory
     - Number of frames is *much* smaller than aggregate number of pages
     - Sufficient to record information *per frame*, not per page and process
       - (For each frame, what page of what process is currently contained?)
   - Obtain frame for page via *hashing* of page number
     - PowerPC, UltraSPARC, IA-64

*** Example
    :PROPERTIES:
    :CUSTOM_ID: inverted-page-table-example
    :reveal_extra_attr: data-audio-src="./audio/9-inverted-page-table.ogg"
    :END:
    - Simplistic example, 4 frames, hashing via ~modulo 4~
      {{{reveallicense("./figures/OS/hail_f0610.pdf.meta","40rh")}}}
      - (Inverted page table below based on Fig. 6.15 of cite:Hai19;
	represents main memory situation shown to the right)
      - E.g., page 0: 0 mod 4 = 0; thus look into row 0, find that page 0
	is contained in frame 1

   |-------+------+---------+-------|
   | Valid | Page | Process | Frame |
   |-------+------+---------+-------|
   |     1 |    0 |      42 |     1 |
   |     1 |    1 |      42 |     0 |
   |     1 |    6 |      42 |     3 |
   |     0 |    X |       X |     X |
   |-------+------+---------+-------|
#+BEGIN_NOTES
Consider the simplified and simplistic inverted page table shown here
capturing the memory situation of the process shown to the right,
which is called process 42.  Note that in reality, RAM would
contain pages of several processes.

Here just 4 frames of RAM are available, and hashing of page number
/n/ is computed as /n modulo 4/.

When, for example, an instruction executed by the CPU on behalf of
process 42 touches a virtual address located in page 0, hashing is
used to compute /0 mod 4 = 0/, which indicates that the first table
entry needs to be accessed (as counting starts from 0).  This entry shows
that page 0 is located in frame 1, and the physical address can be
built as usual.

As a side remark, if you read elsewhere about inverted page tables
please note that you may find a slightly different scheme where the
frame number is not included in table entries: If the table contains
exactly one entry per frame of RAM, frame numbers can be omitted and
instead entry number /n/ would indicate the contents of frame number
/n/.  E.g., entry 2 here would not contain a frame number but directly
indicate that frame 2 contains page 6 of process 42.
#+END_NOTES

*** Observations
    :PROPERTIES:
    :CUSTOM_ID: inverted-page-table-observations
    :END:
    - *Constant* table size
      - Proportional to main memory size
      - Independent of number of processes
	- One entry per frame is sufficient
    - Entries are large
      - Page numbers included (hash collisions)
      - Process IDs included (hash collisions)
      - Pointers for overflow handling necessary (not shown above)
      - If there is one entry per frame, the frame number does not
        need to be included (implicit as entry’s number)
    - (Side note)
      - Efficient use in practice is hard
        - See [[https://yarchive.net/comp/linux/page_tables.html][comments by Linus Torvalds]]
          if you are interested (beyond class topics)

** Hardware Support for Address Translations
   - Lots of architectures support page tables in hardware
     - Multilevel and/or inverted page tables
     - *Page table walker* does translation in hardware
       - Architecture specifies page table structure
	 - For multilevel page tables, special register stores start
           address of page directory
   - Special cache to reduce translation latency, the TLB (next slide)

*** Translation Lookaside Buffer (TLB)
    :PROPERTIES:
    :CUSTOM_ID: tlb
    :END:
    #+INDEX: Translation lookaside buffer (TLB) (Memory II)
    - Access to virtual address may require *several memory accesses*
      → Overhead
      - Access to page table (one per level)
      - Access to data
    - Improvement: [[https://en.wikipedia.org/wiki/Hardware_cache][Caching]]
      - Special cache, called *TLB*, for page table entries
	- Recently used translations of page numbers to frame numbers
      - [[file:Operating-Systems-Memory-I.org::#mmu][MMU]] searches in TLB first to build physical address
	- Note: Search for *page*, not entire virtual address
	- If not found (TLB miss): Page table access
      - Note: Context switch may require TLB flush → Overhead
	- Reduced when entries have address space identifier (ASID)
	  - See cite:Hai19 if you are interested in details

# ** Exercise Task
#    :PROPERTIES:
#    :CUSTOM_ID: exercise-9-1
#    :reveal_data_state: no-toc-progress
#    :reveal_extra_attr: class="jitt"
#    :END:
#     Answer the following questions in {{{jittquiz}}}.

#     The [[#inverted-page-table-observations][observations on inverted page tables]]
#     mention collisions for page numbers and pointers for overflow
#     handling.
#     Extend the sample [[#inverted-page-table-example][main memory situation]]
#     as specified subsequently.  Note that answers to those
#     questions are not contained in the slides, but require
#     constructive work.
#     1. Describe how loading of an additional page into RAM for process
#        42 may lead to a hash collision.  How could the resulting
#        situation be represented in an appropriately extended page
#        table?
#     2. Explain why the presence of multiple processes requires the
#        inclusion of process IDs (or other tags) in inverted page tables.

* Policies

** Terminology
   - To page = to load a page of data into RAM
     - Managed by OS
   - Paging causes [[#swapping][swapping]] and may lead to
     [[#thrashing][thrashing]] as discussed next
   - Paging policies, to be discussed afterwards, aim to reduce both
     phenomena

*** Swapping
    :PROPERTIES:
    :CUSTOM_ID: swapping
    :END:
    #+INDEX: Swapping (Memory II)
    - Under-specified term
    - Either (desktop OSs)
      - Usual paging in case of page faults
	- Page replacement: Swap one page out of frame to disk, another one in
	  - Discussed [[#replacement-policy][subsequently]]
    - Or (e.g., mainframe OSs)
      - Swap out *entire process* (all of its pages and all of its threads)
	- New [[file:Operating-Systems-Scheduling.org::#thread-states][state]] for its threads: swapped/suspended
	  - No thread can run as nothing resides in RAM
      - Swap in later, make process/threads runnable again
      - (Not considered subsequently)

*** Thrashing
    :PROPERTIES:
    :CUSTOM_ID: thrashing
    :END:
    #+INDEX: Thrashing (Memory II)
    - *Permanent* swapping without progress
      - Another type of [[file:Operating-Systems-Interrupts.org::#livelocks][livelock]]
      - Time wasted with *overhead* of swapping and context switching
    - Typical situation: no free frames
      - Page faults are *frequent*
	- OS *blocks* thread, performs *page replacement* via swapping
	- After context switch to different thread, again page fault
      - More swapping
    - Reason: Too many processes/threads
      - Mainframe OSs may swap out entire processes then
	- Control so-called *multiprogramming level* (MPL)
	  - Enforce upper bound on number of *active* processes
      - Desktop OSs let users deal with this

** Fetch Policy
   :PROPERTIES:
   :CUSTOM_ID: fetch-policy
   :END:
   #+INDEX: Demand paging (Memory II)
   #+INDEX: Prepaging (Memory II)
   - General question: When to bring pages into RAM?
   - Popular alternatives
     - *Demand paging* (contrast with [[file:Operating-Systems-Memory-I.org::#demand-loading][demand loading]])
       - Only load page upon *page fault*
       - Efficient use of RAM at cost of lots of page faults
     - *Prepaging*
       - Bring several pages into RAM, anticipate *future use*
       - If future use guessed correctly, fewer page faults result
	 - Also, loading a random hard disk page into RAM involves
           [[https://www.computerhope.com/jargon/r/rotadela.htm][rotational delay]]
	 - Such delays are reduced when neighboring pages are read in
           one operation
         - (Even for SSDs, multiple random I/O operations are slower
           than a single sequential I/O operation of the same size as
           each operation comes with overhead)

*** Prepaging ideas
    - *Clustered paging*, read around
      - Do not read just one page but a cluster of neighboring pages
	- Can be turned on or off in system calls
    - OS and program start
      - OSs may *monitor page faults*, record and use them upon next start
	to pre-load necessary data
	- Linux with [[https://en.wikipedia.org/wiki/Readahead][readahead system call]]
	- Windows with
          [[https://en.wikipedia.org/wiki/Prefetcher][Prefetching and SuperFetch]]

** Replacement Policy
   :PROPERTIES:
   :CUSTOM_ID: replacement-policy
   :END:
   #+INDEX: Page replacement!Goal (Memory II)
   - What frame to re-use when a page fault occurs while all frames
     are full?
   - Recall goal: Keep [[file:Operating-Systems-Memory-I.org::#working-set][working sets]] in RAM
   - Local vs global replacement
     - Local: Replace within frames of same process
       - When to in- or decrease resident set size?
     - Global: Replace among all frames

*** Sample Replacement Policies
    :PROPERTIES:
    :CUSTOM_ID: replacement-policies
    :END:
    #+INDEX: Page replacement!Policies (Memory II)
    #+INDEX: Page replacement!Clock (Memory II)
    #+INDEX: Page replacement!FIFO (Memory II)
    #+INDEX: Page replacement!LRU (Memory II)
    #+INDEX: Page replacement!OPT (Memory II)
    #+INDEX: First in, first out page replacement (Memory II)
    #+INDEX: Least recently used page replacement (Memory II)
    #+INDEX: Optimal page replacement (Memory II)
    - OPT: Hypothetical *optimal* replacement
      - Replace page that has its next use furthest in the future
	- Needs knowledge about future, which is unrealistic
    - FIFO: *First In, First Out* replacement
      - Replace oldest page first
	- Independent of number/distribution of references
    - LRU: *Least Recently Used* replacement
      - Replace page that has gone the longest without being accessed
	- Based on principle of locality, upcoming access unlikely
    - *Clock* (second chance)
      - Replace “unused” page
	- Use 1 bit to keep track of “recent” use

*** Replacement Examples
    :PROPERTIES:
    :CUSTOM_ID: fig-6.19
    :reveal_extra_attr: data-audio-src="./audio/f0619.ogg"
    :END:
    #+INDEX: Page replacement!Examples (Memory II)
    {{{reveallicense("./figures/OS/hail_f0619.pdf.meta","50rh")}}}
#+BEGIN_NOTES
In this comparison of the OPT, LRU, and FIFO replacement policies,
each pair of boxes represents the two frames available on an
unrealistically small system.  The numbers within the boxes indicate
which page is stored in each frame.  The numbers across the top
are the page reference sequence, and the letters h and m indicate hits and
misses.  In this example, LRU performs better than FIFO, in that it
has one more hit.  OPT performs even better, with three hits.
#+END_NOTES

*** Clock (Second Chance)
    :PROPERTIES:
    :CUSTOM_ID: clock
    :END:
    #+INDEX: Clock!Data structure (Memory II)
    - Frames arranged in cycle, *pointer* to next frame
      {{{reveallicense("./figures/OS/8-clock.meta","60rh",nil,none)}}}
      - (Naming: Pointer as hand of *clock*)
      - Pointer “wraps around” from “last” frame to “first” one
    - *Use-bit* per frame
      - Set to 1 when page referenced/used

*** Beware of the Use Bit
    - Use-bit may be part of hardware support
    - Use-bit set to 0 when page swapped in
    - Under demand paging, use-bit immediately set to 1 due to reference
      - Following examples assume that page is referenced for use
      - Thus, use-bit is 1 for new pages
    - Under prepaging, use-bit may stay 0

*** Clock (Second Chance): Algorithm
    :PROPERTIES:
    :CUSTOM_ID: clock-algorithm
    :END:
    #+INDEX: Clock!Algorithm (Memory II)
    - If *page hit*
      - Set use-bit to 1
      - Keep pointer unchanged
    - If *page fault*
      - Check frame at pointer
      - If free, use immediately, advance pointer
      - Otherwise
	- If use-bit is 0, then *replace*; advance pointer
	- If use-bit is 1, reset bit to 0, advance pointer, repeat
          (Go to “Check frame at pointer”)
	- (Naming: In contrast to FIFO, page gets a *second chance*)

*** Clock (Second Chance): Animation
    - Consider reference of page 7 in [[#clock][previous situation]]
      - All frames full, page 7 not present in RAM
      - Page fault
        {{{reveallicense("./figures/OS/clock-steps.meta","50rh",nil,none)}}}
	- Frame at pointer is 2, page 44 has use bit of 1
	  - Reset use bit to 0, advance pointer to frame 3
	- Frame at pointer is 3, page 3 has use bit of 0
	  - Replace page 3 with 7, set use bit to 1 due to reference
	  - Advance pointer to frame 4

*** Clock: Different Animation
    - Situation
      - Four frames of main memory, initially empty
      - Page references: 1, 3, 4, 7, 1, 2, 4, 1, 3, 4

    {{{reveallicense("./figures/OS/clock-steps-2.meta","40rh")}}}

*** More Replacement Examples
    :PROPERTIES:
    :CUSTOM_ID: ex-replacement
    :reveal_extra_attr: data-audio-src="./audio/8-ex-replacement.ogg"
    :END:
    {{{reveallicense("./figures/OS/page-replacement.meta","60rh",nil,none)}}}
#+BEGIN_NOTES
The layout of this diagram mirrors the one of Fig. 6.19 but is
extended to four frames.  For Clock, demand paging is assumed;
the arrow shows the pointer position, and “*” indicates a use-bit
of 1.

Let’s see how Clock works.  Consider the 6th page reference, which is
supposed to bring page 2 into RAM under the situation where
- all frames are full,
- all use-bits are 1,
- the pointer is at frame 0, where page 1 has a use bit of 1.

Following Clock’s steps, the use-bit of page 1 is reset to 0, and the
pointer is advanced to frame 1.
Page 3 in frame 1 has a use-bit of 1, which is reset to 0, and the
pointer is advanced.  That way all use-bits are reset, before the
pointer points to page 1 in frame 0 again.  This time, the use-bit is
0, hence the contents of frame 0 are replaced with page 2, and the
pointer is advanced once more.  As we consider demand paging,
an access into page 2 occurs, which sets the use-bit to 1.
#+END_NOTES

** JiTT Task
   :PROPERTIES:
   :CUSTOM_ID: exercise-9-1
   :reveal_data_state: no-toc-progress
   :reveal_extra_attr: class="jitt"
   :END:

   - Perform the following task in {{{jittquiz}}}.

     Apply the page replacement algorithms OPT, FIFO, LRU, and Clock
     (Second Chance) for four frames of main memory to the following
     stream of page references under demand paging: 1, 3, 4, 7, 1, 2, 4,
     1, 3, 4 @@html:<br>@@
     Verify your results against the [[#ex-replacement][previous slide]]
     and raise any questions that you may have.

* Conclusions

** Summary
   - Virtual memory provides abstraction over RAM and secondary storage
     - Paging as fundamental mechanism for flexibility and isolation
   - Page tables managed by OS
     - Hardware support via MMU with TLB
     - Management of “necessary” pages is complex
       - Tasks include prepaging and page replacement

#+INCLUDE: "backmatter.org"
