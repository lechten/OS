# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+OPTIONS: toc:1

#+TITLE: OS Overview
#+KEYWORDS: operating systems, overview
#+DESCRIPTION: Overview for course on operating systems

* Introduction
** Recall: Big Picture of CSOS
   :PROPERTIES:
   :CUSTOM_ID: csos-big-picture
   :END:
   - Computer Structures and Operating Systems (CSOS)
     #+ATTR_REVEAL: :frag (gray-out appear)
     - CS: How to build a computer from logic gates?
       {{{reveallicense("./figures/gates/nand.meta","6rh")}}}
       - Von Neumann architecture
       - CPU (ALU), RAM, I/O
	 {{{reveallicense("./figures/devices/cpu.meta","12rh")}}}
     - OS: What *abstractions* do Operating Systems provide for
       applications?
       - Processes and threads with scheduling and concurrency, virtual memory
         {{{reveallicense("./figures/screenshots/pong.meta","25rh")}}}
       - What is currently executing why and where, using what
         resources how?

*** OS Responsibilities
    :PROPERTIES:
    :CUSTOM_ID: os-responsibilities
    :END:
    {{{revealimg("./figures/external-non-free/jvns.ca/OS-responsibilities.meta",t,"60rh")}}}

*** Definition of Operating System
    :PROPERTIES:
    :CUSTOM_ID: os-definition
    :END:
    - Definition from cite:Hai19: *Software*
      - that *uses hardware* resources of a computer system
      - to provide support for the *execution of other software*.

    {{{reveallicense("./figures/OS/hail_f0101.pdf.meta","35rh")}}}

** Prerequisite Knowledge
   :PROPERTIES:
   :CUSTOM_ID: os-prerequisites
   :END:
   - Be able to write, compile, and execute small /Java/ programs
     - What is an object?  What is the meaning of ~this~ in Java?
     - How do you execute a program that requires a command line argument?
   - Be able to explain basic /data structures/ (stacks, queues, trees) and
     /algorithms/ (in particular, hashing)
   - Being able to explain the database /transaction concept/ and
     /update anomalies/

* OS Teaching
** OS Part
   :PROPERTIES:
   :CUSTOM_ID: teaching-os-part
   :END:
   - Sharp cut ahead
     - So far, we followed a book with projects to build a computer bottom up
     - Unfortunately, nothing similar exists to build an Operating System
       (OS) bottom up
       - (Or is far too technical for our purposes)
   #+ATTR_REVEAL: :frag appear
   - Thus, the OS part presents major concepts and techniques of modern OSs
     - Some topics build upon each other
     - Some are more isolated
     - Goal: Understand and control your devices
       - Performance
       - Trust

** OS Exercises
   :PROPERTIES:
   :CUSTOM_ID: os-exercises
   :END:
   - While the CS part had weekly projects, this will *not* be the
     case for the OS part
   - Group exercises continue, though

** Textbook(s)
   :PROPERTIES:
   :CUSTOM_ID: os-textbooks
   :END:
   - Major source (adopted in 2017)
     - cite:Hai19 [[https://gustavus.edu/mcs/max/os-book/][Max Hailperin: Operating Systems and Middleware: Supporting Controlled Interaction]]
       - Distributed under the free/libre and open license
         [[https://creativecommons.org/licenses/by-sa/3.0/][CC BY-SA 3.0]]
	 - Source code at [[https://github.com/Max-Hailperin/Operating-Systems-and-Middleware--Supporting-Controlled-Interaction][GitHub]]
       - PDF (also) in Learnweb
   - Additions
     - cite:Sta14 Stallings: Operating Systems -- Internals and Design
       Principles, 8/e, 2014
     - cite:TB15 Tanenbaum, Bos: Modern operating systems, 4th edition, 2015

** Presentations (1/2)
   :PROPERTIES:
   :CUSTOM_ID: os-presentations-1
   :END:
   - HTML presentations with audio for self-study
     - Read [[basic:https://oer.gitlab.io/hints.html][usage hints]] first
   - HTML generated from simple text files in [[beyond:https://orgmode.org/][Org mode]]
     - [[beyond:https://gitlab.com/oer/OS][Source code available on GitLab]]
       - You may want to check out those for your annotations
     - [[basic:https://oer.gitlab.io/OS][Generated presentations]]
       - Those are draft versions until you see a link in Learnweb
     - Offline use possible
       - Generate yourself or
         [[beyond:https://gitlab.com/oer/OS/-/pipelines][download from latest GitLab pipeline]]
     - Open Educational Resources (OER), your contributions are very welcome!

** Presentations (2/2)
   :PROPERTIES:
   :CUSTOM_ID: os-presentations-2
   :END:
   - As usual, (pointers to) presentations are available in Learnweb
   - Presentations include
     - Learning objectives
     - Explanations for class topics
     #+ATTR_REVEAL: :frag appear :audio ./audio/1-presentation-hints.ogg
     - Some slides contain audio explanations, e.g., the one starting
       with this bullet point
#+BEGIN_NOTES
Starting with the final bullet point of this slide, audio controls
should have appeared on the lower left.

The folder icon on the upper right indicates that a
transcript of the audio is available.

You read the usage hints for this type of presentations as instructed
on the previous slide, didn’t you?
#+END_NOTES

* OS Plan
** Big Picture of OS Sessions
   :PROPERTIES:
   :CUSTOM_ID: os-plan
   :reveal_extra_attr: data-audio-src="./audio/OS-overview-1.ogg"
   :END:
    #+INDEX: Kernel!Basic explanation (Overview)
    #+INDEX: Interrupt!Basic explanation (Overview)
    #+INDEX: Scheduling!Basic explanation (Overview)
    #+INDEX: System call!Basic explanation (Overview)
    #+INDEX: Time slicing!Basic explanation (Overview)
   {{{reveallicense("./figures/OS/2022-OS-overview-anim-plain.meta","50rh",nil,none,t)}}}
   #+begin_notes
(Audio for this slide is split into several audio files, one for each
step of the animation.  In contrast, these notes contain a transcript
of all animation steps.)

Although the Hack computer does not have an OS, it
will help to recall briefly how you interact with that machine.
On Hack, you are able to run a single program, where you access
hardware directly.  E.g., reading from the keyboard requires access to
a special memory location that represents the state of the underlying
hardware device.

With OSs, applications no longer have direct access to hardware.
Instead OSs manage applications and their use of hardware.
You will learn that the core part of OSs is called
[[file:Operating-Systems-Introduction.org::#kernel][kernel]],
and each vendor’s kernel comes with a specific interface to provide
functionality to applications, usually in the form of so-called
[[file:Operating-Systems-Introduction.org::#system-calls][system calls]].
E.g., when you use ~System.in~ in Java to read keyboard input,
the Java runtime executes a system call to ask the OS for
keyboard input.

Starting from system calls, we will look into techniques for
[[file:Operating-Systems-Interrupts.org][input/output processing]]
(I/O for short) such as access to the keyboard.  Recall that in Hack
you programmed a loop to wait for keyboard input.  Clearly, such a
loop keeps the CPU busy even if no key is pressed, wasting the
resource CPU if other application could perform useful computations.
Hence, I/O is usually paired with
[[file:Operating-Systems-Interrupts.org::#irq-big-picture][interrupt processing]],
which does not exist in Hack.  Briefly, interrupts indicate the
occurrence of external events, and OSs come with
[[file:Operating-Systems-Interrupts.org::#idt][interrupt handlers]].
Then, for example, keyboard processing code only needs to be executed
when a key was pressed.

In contrast to Hack, OSs manage the execution of several applications,
and each application might contain several so-called
[[file:Operating-Systems-Threads.org][threads]] of execution.  It is
up to application programmers to decide how many threads to use for a
single application (and you will create
[[file:Operating-Systems-Threads.org::#java-threads][threads in
Java]]).

The OS manages all those threads and their
[[file:Operating-Systems-Scheduling.org][scheduling]] for execution
on the CPU (or their parallel execution on multiple CPU cores).
Usually, scheduling mechanisms involve
[[file:Operating-Systems-Scheduling.org][time slicing]], which means
that each thread runs for a brief amount of time before the OS
schedules a different thread for execution.  Such scheduling happens
in intervals of about 10-50ms, creating the illusion of parallelism
even if just a single CPU core exists.  Such time-sliced or parallel
executions are also called
[[file:Operating-Systems-Threads.org::#concurrency][concurrent]] executions.

If shared resources are accessed in concurrent executions, subtle bugs
may arise, a special case of which are update anomalies in database
systems.  The notion of
[[file:Operating-Systems-MX.org][mutual exclusion (MX)]]
generalizes several synchronization mechanisms to overcome concurrency
challenges, and we will look at typical related OS mechanisms and
their use in Java.

Just as in Hack, instructions and code of applications need to be
stored in memory.  Differently from Hack with its Harvard
architecture, code and instructions are stored uniformly in RAM in our
Von Neumann machines, and the OS manages the allocation of RAM.
Importantly, mainstream OSs provide support for
[[file:Operating-Systems-Memory-I.org][virtual]] memory,
which does not exist in Hack, but for which we will see advantages
such as isolation and flexibility.

Furthermore, mainstream OSs offer a
[[file:Operating-Systems-Processes.org::#process-control-block][process]]
concept as abstraction for applications, under which several related
and cooperating threads share resources (such as virtual memory or
files).  Finally, OSs offer various
[[file:Operating-Systems-Security.org::#safety-security][security]]
mechanisms for processes and applications, a selection of which will
be topics for the final presentation.

To sum up, this figure visualizes
- what OS topics will be discussed when and
- how topics build upon each other.

Note that some parts of this figure are hyperlinked to other
presentations, which the mouse pointer should indicate.
   #+end_notes

** A Quiz
#+REVEAL_HTML: <script data-quiz="quizOverview" src="./quizzes/overview.js"></script>

#+INCLUDE: "backmatter.org"
