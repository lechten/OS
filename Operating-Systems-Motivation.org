# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2018-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+OPTIONS: toc:1

#+TITLE: OS Motivation
#+KEYWORDS: operating system, control, freedom, free software, license
#+DESCRIPTION: Discussion of Operating System as software that controls devices

* Introduction

** Learning Objectives
   - Discuss role of OSs to control computers
     - Performance aspects
     - Ownership and control
   - Discuss role of licenses to control software

** Performance Aspects
   - OS manages computer’s resources: CPU, memory, I/O
   - OS understanding helps to identify and reason about resource
     bottlenecks.
     - Improve design, analysis, and implementation of information systems.
     - E.g., why is my computer/application slow?  How to improve that?
   #+ATTR_REVEAL: :frag appear
   - (Above are “traditional” topics for OS courses; this presentation
     takes a different direction.)

** Whom do Computers Obey?
   - Recall (long-term) goal of CSOS
     {{{reveallicense("./figures/devices/laptop-154091_1280.meta","15rh")}}}
     - “Play and experiment with and control any computer, at any level
       of interest”
   #+ATTR_REVEAL: :frag (appear)
   - In CS part, you learned how to build a computer.
     #+ATTR_REVEAL: :frag (appear)
     - How does that feel?
   - In OS part, we investigate Operating Systems (OSs) to control
     computers.
     {{{reveallicense("./figures/3d-man/search-1013910_1920.meta","20rh")}}}
     #+ATTR_REVEAL: :frag (appear)
     - OS controls what is executing when.
     - Who controls the OS?
     - Who controls the computer, then?

** Computers
   :PROPERTIES:
   :CUSTOM_ID: computers
   :END:

{{{revealgrid(42,"./figures/devices/computer.grid",60,4,3,"\"ga1 ga2 ga2 ga3\" \"ga1 ga4 ga5 ga6\" \"ga7 ga8 ga9 ga9\"")}}}

** Enlightenment
#+BEGIN_QUOTE
[[basic:https://en.wikipedia.org/wiki/Answering_the_Question:_What_is_Enlightenment%3F][Enlightenment is man's emergence from his self-incurred immaturity.]]
(Immanuel Kant)
#+END_QUOTE
   #+ATTR_REVEAL: :frag (appear)
   - Your computer does whatever it is programmed to do.
   - You can succumb to somebody else’s programming.
   - Or emerge.

* Choices and Consequences

** Impact of Choices
   #+ATTR_REVEAL: :frag (appear)
   - *Economic* and *ecological/social* impact
     {{{reveallicense("./figures/3d-man/teacher-1014055_1920.meta","20rh")}}}
     - What do you buy/use/create?
       - What do you advertise?
       - What do you impose on others?
   - Economic impact
     - Should be obvious
   - Ecological/social impact
     {{{reveallicense("./figures/3d-man/customer-magnet-1019871_1920.meta","20rh")}}}
     - If you use some social/communication service, you increase its
       appeal and value
       - [[https://en.wikipedia.org/wiki/Network_effect][Network effects]],
         feedback loops, externalities
     - If you *choose* communication under
       [[https://en.wikipedia.org/wiki/Surveillance_capitalism][surveillance]],
       you *impose* surveillance on your family, friends, colleagues …

** Rationale
   - If you want to *own* a device containing a computer (“smart” anything,
     recall [[#computers][slide on computers]]), you need to control
     its software.
     - (Including firmware and underlying hardware)
       - (Topic in its own right)
   #+ATTR_REVEAL: :frag appear
   - Otherwise,
     {{{reveallicense("./figures/art/art-3040484_1280.meta","20rh")}}}
     #+ATTR_REVEAL: :frag (appear)
     - device may work against you, see [[https://codecurmudgeon.com/wp/iot-hall-shame/][IoT Hall of Shame]],
       - e.g.,
	 [[https://news.drweb.com/show/?lng=en&i=11749&c=5][smartphones]] and [[https://en.wikipedia.org/wiki/Volkswagen_emissions_scandal][diesel engines]],
     - device may stop working any time,
       - e.g., useless fitness tracker
         ([[https://support.microsoft.com/en-us/help/4467073/end-of-support-for-the-microsoft-health-dashboard-applications][2019]]),
         bricked smart home
         [[https://www.engadget.com/2017-11-09-logitech-will-brick-harmony-link-in-march.html][entertainment device (2017)]]
	 and [[https://www.theguardian.com/technology/2016/apr/05/revolv-devices-bricked-google-nest-smart-home][hub (2016)]],
	 [[https://www.nytimes.com/2009/07/18/technology/companies/18amazon.html][Orwell books removed from Kindle (2009)]],
     - device may stop receiving (security) updates any time.

* Software

** Free Software
   :PROPERTIES:
   :CUSTOM_ID: free-software
   :END:
   #+INDEX: Free software (Motivation)
   - [[https://www.gnu.org/philosophy/floss-and-foss.en.html][Free Software = Free/Libre and Open Source Software (FLOSS)]]
     {{{reveallicense("./figures/people/Richard_Stallman_at_Pittsburgh_University.meta","15rh")}}}
     - Term coined by Richard Stallman, [[https://www.fsf.org/][Free Software Foundation]]
     - “Free” as in free speech or freedom, not free beer
   - [[https://www.gnu.org/philosophy/free-sw.html][Four freedoms]]
     1. Run software (also changed versions)
     2. Study software
     3. Redistribute copies
     4. Distribute modified versions

** Free vs Open Source
   :PROPERTIES:
   :CUSTOM_ID: floss
   :END:
   #+INDEX: FLOSS (Motivation)
   - Whether software is free (libre) or open source (or both, FOSS,
     FLOSS) or something else, depends on the *license*
     #+ATTR_REVEAL: :frag appear
     - 1986: Free software defined
       - See [[https://www.gnu.org/bulletins/bull1.txt][GNU’s Bulletin 1]]
         for origin
     - 1998: [[https://opensource.org/][Open Source Software (OSS)]] defined
     - See [[https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses][Comparison of free and open-source software licenses]]
       - Well-known FLOSS licenses include Apache License, GNU Public
         License, Eclipse Public License, Mozilla Public License
   #+ATTR_REVEAL: :frag appear
   - Mostly philosophical distinction, sometimes religious
     - I prefer the term “free software” because it emphasizes freedom
     - If you do not want *your* academic works (theses, code,
       project results) to disappear, publish them under free licenses

** GNU/Linux
   :PROPERTIES:
   :CUSTOM_ID: gnu-linux
   :END:
   #+INDEX: GNU/Linux (Motivation)
   - [[https://www.kernel.org][Linux]] is a [[#free-software][free]] [[file:Operating-Systems-Introduction.org::#os-boundary][OS kernel]]
     {{{reveallicense("./figures/logos/204px-Tux.svg.meta")}}}
   - GNU/Linux is a family of [[#free-software][free]] OSs
     - [[https://en.wikipedia.org/wiki/GNU/Linux_naming_controversy][Naming controversy]]
     #+ATTR_REVEAL: :frag (appear)
     - I encourage you to try out GNU/Linux, which is free software,
       - the major OS for cloud infrastructures,
       - the server OS in lots of *project seminars*.
     - Upcoming *assignments* are based on GNU/Linux
       - Several GNU/Linux distributions can be started as live systems from
         CD/DVD or USB stick without changing your current installation
	 - [[https://www.getgnulinux.org/en/switch_to_linux/][Help on getting started]]

** Firmware
   :PROPERTIES:
   :CUSTOM_ID: firmware
   :END:
   - Firmware = Software that is embedded in hardware by vendor
     - Stored in (EP)ROM, flash
     - Initialization and control of hardware
       - E.g., [[https://en.wikipedia.org/wiki/BIOS][BIOS]],
         [[https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface][(U)EFI]];
         but also video BIOS of graphics card, Management Engine of
         Intel CPUs
         - Typically shipped as binary blobs
         - [[https://en.wikipedia.org/wiki/NSA_ANT_catalog][NSA ANT catalog]]
           also contains firmware trojans (in wake of Snowden revelations)

*** CPU Rings
    :PROPERTIES:
    :CUSTOM_ID: cpu-rings
    :END:
    - Outlook: CPUs have [[file:Operating-Systems-Interrupts.org::#kernel-mode][rings/privilege levels]]
      - Instruction set restricted depending on ring
        - Ring 3: User programs (I/O and memory access restricted)
        - Ring 2, 1: Usually unused (originally for system services
          and device drivers)
        - Ring 0: OS kernel (traditionally, ring 0 was most privileged)
        - More rings cite:Fra19
          - Ring -1: Hypervisor
            ([[../oer-courses/vm-neuland/Docker.org::#virtualization-definition][virtual machine monitor]])
          - Ring -2: System management mode (SMM), unified extensible firmware interface (UEFI)
          - Ring -3: Management Engine

*** Free and Open Firmware
    :PROPERTIES:
    :CUSTOM_ID: free-firmware
    :END:
    - Articles by Jessie Frazelle
      - cite:Fra19
        - “Between Ring -2 and Ring -3 there are at least two and a half other kernels in our stack that have many capabilities. Each of these kernels has its own networking stacks and web servers, which is unnecessary and potentially dangerous, especially if you do not want these rings reaching out over the network to update themselves. The code can also modify itself and persist across power cycles and reinstalls. There is very little visibility into what the code in these rings is actually doing, which is horrifying, considering these rings have the most privileges.”
      - cite:Fra20a
        - “It's an alarming problem that the code running with the most privilege has the least visibility and inspectability.”
      - cite:Fra20b
        - “If you would like to help with the open source firmware movement, push back on your vendors and platforms you are using to make their firmware open source.”

* Conclusions
** Summary
   - Free software is a necessary precondition to control computers
     #+ATTR_REVEAL: :frag appear
     - … and everything embedding a computer
       #+ATTR_REVEAL: :frag appear
       - … which is a lot in times of [[https://en.wikipedia.org/wiki/Ubiquitous_computing][ubiquitous computing]]
         and [[https://en.wikipedia.org/wiki/Internet_of_things][IoT]]!
        {{{reveallicense("./figures/Internet/Internet_of_Things.meta","40rh")}}}
   #+ATTR_REVEAL: :frag appear
   - Do you care?

#+INCLUDE: "backmatter.org"

# Local Variables:
# indent-tabs-mode: nil
# End:
